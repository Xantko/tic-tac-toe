package org.common;

import org.apache.log4j.Logger;
import org.common.buttons.ArrayOfButtons;
import org.common.mainFrame.MainFrame;
import org.common.smallFrames.EndGameDialog;

import java.util.Arrays;

public class WinningService {
    private static final Logger LOGGER = Logger.getLogger(WinningService.class);

    private static int getNumberOfEmptyButtons() {
        return numberOfEmptyButtons;
    }

    public static void setNumberOfEmptyButtons(int numberOfEmptyButtons) {
        WinningService.numberOfEmptyButtons = numberOfEmptyButtons;
    }

    private static int numberOfEmptyButtons = 0;
    private String textForEndGameDialog;
    public void setEndGameText(String s){
        textForEndGameDialog = s;
    }

    public  void resultOfWin(ArrayOfButtons arrayOfButtons) {
        setNumberOfEmptyButtons(getNumberOfEmptyButtons() + 1);

        int[] victoryButtons = new int[3];
        Arrays.fill(victoryButtons, -1);

        // horizontal line
        for (int i = 0; i < 3; i++) {
            int j = 0;
            if (arrayOfButtons.isEqual(i, j, i, j + 1) &&
                    arrayOfButtons.isEqual(i, j, i, j + 2) &&
                    !arrayOfButtons.isEmpty(i, j)) {
                victoryButtons[0] = i * 3 + j;
                victoryButtons[1] = i * 3 + j + 1;
                victoryButtons[2] = i * 3 + j + 2;
                break;
            }
        }

        //vertical line
        for (int j = 0; j < 3; j++) {
            int i = 0;
            if (arrayOfButtons.isEqual(i, j, i + 1, j) &&
                    arrayOfButtons.isEqual(i, j, i + 2, j) &&
                    !arrayOfButtons.isEmpty(i, j)) {
                victoryButtons[0] = i * 3 + j;
                victoryButtons[1] = (i + 1) * 3 + j;
                victoryButtons[2] = (i + 2) * 3 + j;
                break;
            }
        }

        //diagonal 1
        if (arrayOfButtons.isEqual(0, 0,1, 1) &&
                arrayOfButtons.isEqual(0, 0,2, 2) &&
                !arrayOfButtons.isEmpty(0, 0)) {
            victoryButtons[0] = 0;
            victoryButtons[1] = 4;
            victoryButtons[2] = 8;
        }

        //diagonal 2
        if (arrayOfButtons.isEqual(0, 2, 1, 1) &&
                arrayOfButtons.isEqual(0, 2, 2, 0) &&
                !arrayOfButtons.isEmpty(0, 2)) {
            victoryButtons[0] = 2;
            victoryButtons[1] = 4;
            victoryButtons[2] = 6;
        }


        EndGameDialog endGameDialog;
        LOGGER.info("Setting victoryButtons");
        if (victoryButtons[0] != -1){
            for (int buttonNumber: victoryButtons){
                arrayOfButtons.setVictoryButton(buttonNumber / 3, buttonNumber % 3);
            }
            endGameDialog = new EndGameDialog(MainFrame.getInstance());
            endGameDialog.setTextForJLabelOfWin(textForEndGameDialog);
            endGameDialog.setVisible(true);
            setNumberOfEmptyButtons(0);
        } else if (getNumberOfEmptyButtons() == 9) {
            setNumberOfEmptyButtons(0);
            endGameDialog = new EndGameDialog(MainFrame.getInstance());
            endGameDialog.setTextForJLabelOfWin("Draw");
            endGameDialog.setVisible(true);
        }
    }

}