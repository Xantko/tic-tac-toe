package org.common;

public interface Player {

    enum PlayerMode {
        CLIENT_MODE, SERVER_MODE, LOCAL_MODE
    }

    void beginGame();

    void endGame();

}
