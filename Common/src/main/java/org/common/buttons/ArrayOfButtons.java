package org.common.buttons;

import org.apache.log4j.Logger;
import org.common.mainFrame.MainPanel;

import java.util.ArrayList;

public class ArrayOfButtons {
    private static final Logger LOGGER = Logger.getLogger(ArrayOfButtons.class);
    private static int GRID_WIDTH = 3;
    private static int GRID_HEIGHT = 3;

    private ArrayList<ArrayList<ButtonForGame>> innerArrayOfButtons = new ArrayList<>();

    private ArrayOfButtons() {
        LOGGER.info("Starting to create array of buttons");
        for (int i = 0; i < GRID_HEIGHT; i++) {
            innerArrayOfButtons.add(new ArrayList<>());
            for (int j = 0; j < GRID_WIDTH; ++j) {
                innerArrayOfButtons.get(i).add((ButtonForGame) ButtonForGame.getButtonForGame());
                MainPanel.getInstance().add(innerArrayOfButtons.get(i).get(j));

            }
        }
    }

    public void resetOfButtons(){
        LOGGER.info("Resetting buttons");
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                innerArrayOfButtons.get(i).get(j).setDefaultButtons();
            }
        }
    }

    public ArrayList<ArrayList<ButtonForGame>> getInnerArrayOfButtons() {
        return innerArrayOfButtons;
    }

    public ButtonForGame getButtonByCoordinates(int i, int j) {
        return innerArrayOfButtons.get(i).get(j);
    }

    public boolean isEqual(int iFirst, int jFirst, int iSecond, int jSecond){
        return innerArrayOfButtons.get(iFirst).get(jFirst).getText().equals(
                innerArrayOfButtons.get(iSecond).get(jSecond).getText());
    }

    public boolean isEmpty(int i, int j){
        return innerArrayOfButtons.get(i).get(j).getText().isEmpty();
    }

    public void setVictoryButton(int i, int j){
        innerArrayOfButtons.get(i).get(j).setVictoryButton();
    }

    // -----------------------------------------------------------------------------------

    private static ArrayOfButtons arrayOfButtons = new ArrayOfButtons();

    public static ArrayOfButtons getInstance() {
        return arrayOfButtons;
    }
}
