package org.common.buttons;

import javax.swing.*;
import java.awt.*;

/**
 * Created by Alex on 05.10.2017.
 */
public class ButtonForGame extends JButton {
    protected static int i = 0;

    private int[] id;

    protected ButtonForGame() {
        this.setPreferredSize(new Dimension(100, 100));
        Font font = new Font("Arial", Font.BOLD, 85);
        this.setFont(font);
        this.setForeground(Color.BLACK);
        this.setFocusable(false);
    }

    public void setDefaultButtons(){
        this.setText("");
        this.setForeground(Color.BLACK);
    }

    public static JButton getButtonForGame() {
        return new ButtonForGame();
    }

    public void setVictoryButton() {
        this.setForeground(Color.RED);
    }

    public int[] getId() {
        return id;
    }

    public void setId(int[] id) {
        this.id = id;
    }

}
