package org.common;

import org.common.buttons.ArrayOfButtons;

import java.util.Arrays;

public class GameMessage {

    private int[] cellIndex;
    private String cellValue;
    private boolean cellAvailable;

    private boolean isCellAvailable() {
        return cellAvailable;
    }

    public void setCellAvailable(boolean cellAvailable) {
        this.cellAvailable = cellAvailable;
    }

    private int[] getCellIndex() {
        return cellIndex;
    }

    public void setCellIndex(int[] cellIndex) {
        this.cellIndex = cellIndex;
    }

    private String getCellValue() {
        return cellValue;
    }

    public void setCellValue(String cellValue) {
        this.cellValue = cellValue;
    }

    public GameMessage() {
        this.cellIndex = new int[]{0, 0};
        this.cellValue = "X";
        this.cellAvailable = false;
    }

    public GameMessage(int[] cellIndex, String cellValue, boolean cellAvailable) {
        this.cellValue = cellValue;
        this.cellIndex = cellIndex;
        this.cellAvailable = cellAvailable;
    }

    public String toString() {
        return "GameMessage: " + Arrays.toString(cellIndex) + " " + cellValue;
    }

    public boolean processMessage(){
        int[] index = this.getCellIndex();
        ArrayOfButtons.getInstance().getButtonByCoordinates(index[0], index[1]).setText(this.getCellValue());
        return this.isCellAvailable();
    }

}
