package org.common.mainFrame;

import javax.swing.*;
import java.awt.*;

/**
 * Created by Alex on 05.10.2017.
 */
public class MainPanel extends JPanel {

    private static MainPanel instance = new MainPanel();

    private MainPanel() {
        super();
        this.setSize(300, 300);
        this.setMinimumSize(new Dimension(300, 300));
        this.setLayout(new GridLayout(3, 3));
    }

    public static MainPanel getInstance() {
        return instance;
    }
}
