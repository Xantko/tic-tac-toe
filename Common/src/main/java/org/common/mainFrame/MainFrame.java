package org.common.mainFrame;

import org.apache.log4j.Logger;
import org.common.WinningService;
import org.common.buttons.ArrayOfButtons;
import org.common.smallFrames.PlayerModeDialog;

import javax.swing.*;

/**
 * Created by Alex on 04.10.2017.
 */
public class MainFrame extends JFrame {
    private static final Logger LOGGER = Logger.getLogger(MainFrame.class);

    private static MainFrame singleMainFrame = new MainFrame();

    private MainFrame() {
        super();
        this.setVisible(true);
        this.setTitle("Tic-Tac-Toe");
        this.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        this.setSize(300, 300);
        this.setLocationRelativeTo(null);
        this.setResizable(false);
        this.add(MainPanel.getInstance());
        this.setJMenuBar(mainMenu());
        this.revalidate();
        this.pack();
    }

    private JMenuBar mainMenu() {
        JMenuBar mainMenu = new JMenuBar();
        JMenu file = new JMenu("File");
        mainMenu.add(file);
        JMenuItem exit = new JMenuItem("Exit");
        JMenuItem backToMainMenu = new JMenuItem("Main menu...");
        file.add(backToMainMenu);
        file.addSeparator();
        file.add(exit);

        backToMainMenu.addActionListener(e -> {
            LOGGER.info("Pressed 'backToMenu' button on JMenuBar");
            WinningService.setNumberOfEmptyButtons(0);
            ArrayOfButtons.getInstance().resetOfButtons();
            PlayerModeDialog.getInstance().setVisible(true);
        });

        exit.addActionListener(e -> System.exit(0));
        exit.setAccelerator(KeyStroke.getKeyStroke("ctrl X"));

        return mainMenu;
    }

    public static MainFrame getInstance() {
        return singleMainFrame;
    }
}
