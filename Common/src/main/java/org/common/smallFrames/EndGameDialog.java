package org.common.smallFrames;

import org.apache.log4j.Logger;
import org.common.WinningService;
import org.common.buttons.ArrayOfButtons;
import org.common.mainFrame.MainFrame;

import javax.swing.*;
import java.awt.*;

/**
 * Created by Alex on 27.11.2017.
 */
public class EndGameDialog extends JDialog {
    private static final Logger LOGGER = Logger.getLogger(EndGameDialog.class);

    private JLabel jLabel;

    public void setTextForJLabelOfWin(String s) {
        jLabel.setText(s);
    }

    public EndGameDialog(JFrame owner) {
        super(owner, "Game over!", true);
        setLocationRelativeTo(owner);
        jLabel = new JLabel();
        jLabel.setHorizontalAlignment(SwingConstants.CENTER);
        jLabel.setVerticalAlignment(SwingConstants.CENTER);
        add(jLabel, BorderLayout.CENTER);
        JButton replay = new JButton("Replay");
        replay.addActionListener(e -> methodForResetSinglePlayer());
        JButton exit = new JButton("Exit");
        exit.addActionListener(e -> System.exit(0));
        JButton backToMenu = new JButton("Back to menu");
        backToMenu.addActionListener(e -> {
            LOGGER.info("Pressed button 'Back to menu' and opened PlayerModeDialog");
            methodForResetSinglePlayer();
            this.setVisible(false);
            PlayerModeDialog.getInstance().setVisible(true);

        });
        JPanel panel = new JPanel();
        panel.add(replay);
        panel.add(backToMenu);
        panel.add(exit);
        add(panel, BorderLayout.SOUTH);
        setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
        setSize(300, 110);
        setLocationRelativeTo(owner);
    }

    public static EndGameDialog getInstance() {
        return new EndGameDialog(MainFrame.getInstance());
    }

    private void methodForResetSinglePlayer() {
        WinningService.setNumberOfEmptyButtons(0);
        ArrayOfButtons.getInstance().resetOfButtons();
        setVisible(false);
    }

}