package org.common.smallFrames;

import org.common.WinningService;
import org.common.buttons.ArrayOfButtons;
import org.common.mainFrame.MainFrame;

import javax.swing.*;
import java.awt.*;

public class DisconnectDialog extends JDialog{
    private static DisconnectDialog instance = null;
    private JLabel jLabel;
    private DisconnectDialog(JFrame owner){
        super(owner, "Connection error!", true);
         jLabel = new JLabel();
        jLabel.setHorizontalAlignment(SwingConstants.CENTER);
        jLabel.setVerticalAlignment(SwingConstants.CENTER);
        add(jLabel, BorderLayout.CENTER);
        JButton ok = new JButton("OK");
        ok.addActionListener(e -> {
            setVisible(false);
            WinningService.setNumberOfEmptyButtons(0);
            ArrayOfButtons.getInstance().resetOfButtons();
            PlayerModeDialog.getInstance().setVisible(true);
        });
        JPanel jPanel = new JPanel();
        jPanel.add(ok);
        add(jPanel, BorderLayout.SOUTH);
        setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
        setSize(170, 110);
        setLocationRelativeTo(owner);
    }
    public void setjLabelText(String text){
        jLabel.setText(text);
    }

    public static synchronized DisconnectDialog getInstance(){
        if (instance == null) {
            instance = new DisconnectDialog(MainFrame.getInstance());
        }
        return instance;
    }
}