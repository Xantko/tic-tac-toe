package org.common.smallFrames;

import org.apache.log4j.Logger;
import org.common.Player;
import org.common.PlayerCreator;
import org.common.mainFrame.MainFrame;

import javax.swing.*;
import java.awt.*;


import static org.common.Player.*;

public class PlayerModeDialog extends JDialog implements PlayerCreator {

    private static final Logger LOGGER = Logger.getLogger(PlayerModeDialog.class);

    private static PlayerModeDialog playerModeDialog = null;

    public void reInitPlayer(PlayerMode playerMode){
        LOGGER.info("Reinitialization mode");
        if (player != null){
            player.endGame();
        }
        player = getPlayer(playerMode);
        player.beginGame();
    }

    private Player player = null;

    protected PlayerModeDialog(MainFrame owner) {
        super(owner, "Choose a mode", true);
        JButton localGame = new JButton("Local game");
        localGame.setPreferredSize(new Dimension(130, 30));
        JButton multiplayer = new JButton("Multiplayer");
        multiplayer.setPreferredSize(new Dimension(130, 30));
        localGame.addActionListener(e -> {
            setVisible(false);
            LOGGER.info("Changing player mode to LOCAL_MODE");
            reInitPlayer(PlayerMode.LOCAL_MODE);
        });
        multiplayer.addActionListener(e -> {
            setVisible(false);
            LOGGER.info("Opening ServerClientDialog");
            ServerClientDialog.getInstance().setVisible(true);
        });
        JButton exit = new JButton("Exit");
        exit.setPreferredSize(new Dimension(130, 30));
        exit.addActionListener(e -> System.exit(0));
        JPanel jPanel = new JPanel();
        jPanel.add(localGame, BorderLayout.NORTH);
        jPanel.add(multiplayer, BorderLayout.CENTER);
        jPanel.add(exit, BorderLayout.SOUTH);
        add(jPanel);
        setResizable(false);
        setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
        setSize(190, 160);
        setLocationRelativeTo(owner);
    }

    public static PlayerModeDialog getInstance() {
        if (playerModeDialog == null){
            playerModeDialog = new PlayerModeDialog(MainFrame.getInstance());
        }
        return playerModeDialog;
    }

    public static void setInstance(PlayerModeDialog pModeDialog){
        playerModeDialog = pModeDialog;
    }

}
