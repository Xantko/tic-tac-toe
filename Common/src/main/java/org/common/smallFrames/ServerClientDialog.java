package org.common.smallFrames;


import org.apache.log4j.Logger;
import org.common.PlayerCreator;

import javax.swing.*;



import static org.common.Player.PlayerMode.*;

public class ServerClientDialog extends JDialog implements PlayerCreator{
    private static final Logger LOGGER = Logger.getLogger(ServerClientDialog.class);

    private static ServerClientDialog instance = null;

    public static ServerClientDialog getInstance(){
        if (instance == null) {
            instance = new ServerClientDialog(PlayerModeDialog.getInstance());
        }
        return instance;
    }

    private ServerClientDialog(JDialog owner) {
        super(owner, "Server or Client", true);
        JButton server = new JButton("Create a game");
        JButton client = new JButton("Connect to game");
        JButton back = new JButton("Back");
        JPanel jPanel = new JPanel();
        jPanel.add(server);
        jPanel.add(client);
        jPanel.add(back);
        server.addActionListener(e -> {
            LOGGER.info("Changing player mode to SERVER_MODE");
            setVisible(false);
            PlayerModeDialog.getInstance().reInitPlayer(SERVER_MODE);
        });
        client.addActionListener(e -> {
            LOGGER.info("Changing player mode to CLIENT_MODE");
            this.setVisible(false);
            PlayerModeDialog.getInstance().reInitPlayer(CLIENT_MODE);
        });
        back.addActionListener(e -> {
            LOGGER.info("Opening PlayerModeDialog");
            setVisible(false);
            PlayerModeDialog.getInstance().setVisible(true);
        });
        add(jPanel);
        setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
        setLocationRelativeTo(owner);
        setSize(150, 150);
    }
}
