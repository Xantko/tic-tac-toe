package org.server;


import org.common.mainFrame.MainFrame;
import org.common.smallFrames.PlayerModeDialog;

import javax.swing.*;
import java.awt.*;


public class WaitingDialog extends JDialog {

    private static WaitingDialog waitingDialog = null;

    private WaitingDialog(JFrame owner) {
        super(owner, "Connection", true);
        JLabel jLabel = new JLabel("Waiting connection...");
        jLabel.setHorizontalAlignment(SwingConstants.CENTER);
        jLabel.setVerticalAlignment(SwingConstants.CENTER);
        add(jLabel, BorderLayout.CENTER);
        JButton cancel = new JButton("Cancel");

        cancel.addActionListener(e -> {
            setVisible(false);
            MainServerSocket.getInstance().closeServerSocket();
            PlayerModeDialog.getInstance().setVisible(true);
        });
        JPanel panel = new JPanel();
        panel.add(cancel);
        add(panel, BorderLayout.SOUTH);
        setSize(170, 110);
        setLocationRelativeTo(owner);
    }

    public static WaitingDialog getInstance() {
        if (waitingDialog == null)
            waitingDialog = new WaitingDialog(MainFrame.getInstance());
        return waitingDialog;
    }
}
