package org.server;

import com.google.gson.Gson;
import org.apache.log4j.Logger;
import org.common.*;
import org.common.buttons.ArrayOfButtons;
import org.common.buttons.ButtonForGame;
import org.common.mainFrame.MainFrame;
import org.common.smallFrames.DisconnectDialog;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class MainServer implements Player {
    private static final Logger LOGGER = Logger.getLogger(MainServer.class);
    private Gson gson = new Gson();
    private ArrayOfButtons arrayOfButtons = ArrayOfButtons.getInstance();
    private MainServerSocket mainServerSocket = null;
    private boolean isCellAvailable = true;
    private Action action = new Action();
    private WinningService winningService;

    @Override
    public void beginGame() {
        LOGGER.info("Starting beginGame Server mode");
        MainFrame.getInstance().setTitle("Tic-Tac-Toe (Server)");
        winningService = new WinningService();
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; ++j) {
                int[] id = {i, j};
                arrayOfButtons.getButtonByCoordinates(i, j).setId(id);
                arrayOfButtons.getButtonByCoordinates(i, j).addActionListener(action);
            }
        }
        new SocketThread().start();
        WaitingDialog.getInstance().setVisible(true);
    }

    @Override
    public void endGame() {
        LOGGER.info("Starting endGame Server mode");
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                for (ActionListener al : ArrayOfButtons.getInstance().getButtonByCoordinates(i, j).getActionListeners()) {
                    ArrayOfButtons.getInstance().getButtonByCoordinates(i, j).removeActionListener(al);
                }
            }
        }
        MainServerSocket.getInstance().closeServerSocket();

    }

    private class Action implements ActionListener {

        GameMessage gameMessage = new GameMessage();

        @Override
        public void actionPerformed(ActionEvent e) {
            LOGGER.info("Pressed button Server mode");
            ButtonForGame jButton = (ButtonForGame) e.getSource();
            if ((jButton.getText().isEmpty()) && isCellAvailable) {
                jButton.setText("X");
                gameMessage.setCellAvailable(isCellAvailable);
                gameMessage.setCellIndex(jButton.getId());
                gameMessage.setCellValue(jButton.getText());
                mainServerSocket.sendMessage(gson.toJson(gameMessage));
                winningService.setEndGameText("You win!");
                winningService.resultOfWin(arrayOfButtons);
                isCellAvailable = false;
                new AcceptThread().start();
            }
        }
    }

    private class AcceptThread extends Thread {
        @Override
        public void run() {
            try {
                GameMessage gameMessage = gson.fromJson(mainServerSocket.acceptMessage(), GameMessage.class);
                LOGGER.info("Accepting message from Client");
                isCellAvailable = gameMessage.processMessage();
                winningService.setEndGameText("You lose!");
                winningService.resultOfWin(arrayOfButtons);
            } catch (Exception e) {
                LOGGER.warn("Disconnecting Client");
                DisconnectDialog.getInstance().setjLabelText("Client disconnected!");
                DisconnectDialog.getInstance().setVisible(true);
            }
        }
    }

    private class SocketThread extends Thread {
        @Override
        public void run() {
                mainServerSocket = MainServerSocket.getInstance();
                mainServerSocket.waitingConnection();
        }
    }
}
