package org.server;

import org.apache.log4j.Logger;

import java.io.IOException;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.Scanner;

/**
 * Created by Alex on 07.12.2017.
 */


public class MainServerSocket extends ServerSocket {
    private static final Logger LOGGER = Logger.getLogger(MainServerSocket.class);
    private static MainServerSocket server = null;

    private Socket socket;
    private PrintWriter printWriter;
    private Scanner scanner;

    private MainServerSocket(int port) throws IOException {
        super(port);
    }

    public void closeServerSocket() {
        try {
            LOGGER.info("ServerSocket closed");
            if (server != null && !server.isClosed()) {
                server.close();
                server = null;
            }
            if (socket != null && !socket.isClosed()){
                socket.close();
            }
        } catch (IOException e) {
            LOGGER.error(e.getMessage());
        }
    }

    public void waitingConnection() {
        try {
            LOGGER.info("Opening server socket and accepting connection");
            socket = server.accept();
            printWriter = new PrintWriter(socket.getOutputStream(), true);
            scanner = new Scanner(socket.getInputStream());
            WaitingDialog.getInstance().setVisible(false);
        } catch (IOException ex) {
            LOGGER.error("Can't accept. Setting null for Server socket");
            server = null;
        }
    }

    public void sendMessage(String s) {
        printWriter.println(s);
    }

    public String acceptMessage() {
        return scanner.nextLine();
    }

    public static synchronized MainServerSocket getInstance() {
        if (server == null) {
            try {
                server = new MainServerSocket(8584);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return server;
    }

    public boolean isConnect() {
        return socket.isBound();
    }

}
