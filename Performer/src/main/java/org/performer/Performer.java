package org.performer;

import org.client.MainClient;
import org.common.Player;
import org.common.buttons.ArrayOfButtons;
import org.common.mainFrame.MainFrame;
import org.common.smallFrames.PlayerModeDialog;
import org.server.MainServer;
import org.single.MainLocalGame;

public class Performer {

   public static ArrayOfButtons arrayOfButtons = ArrayOfButtons.getInstance();

    public static void main(String[] args) {
        PlayerModeDialog playerModeDialog = new PlayerModeDialog(MainFrame.getInstance()){
            @Override
            public Player getPlayer(Player.PlayerMode mode){
                switch (mode) {
                    case CLIENT_MODE:
                        return new MainClient();
                    case SERVER_MODE:
                        return new MainServer();
                    case LOCAL_MODE:
                        return new MainLocalGame();
                }
                return new MainLocalGame();
            }
        };
        PlayerModeDialog.setInstance(playerModeDialog);
        playerModeDialog.setVisible(true);
    }

}