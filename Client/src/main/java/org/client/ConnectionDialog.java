package org.client;

import org.common.mainFrame.MainFrame;
import org.common.smallFrames.PlayerModeDialog;

import javax.swing.*;
import java.awt.*;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;

public class ConnectionDialog extends JDialog {

    private static ConnectionDialog connectionDialog = null;
    private JLabel jLabel1;
    private boolean isBreak = false;

    private String text = null;

    private ConnectionDialog(JFrame owner) {
        super(owner, "Connection", true);
        JLabel jLabel = new JLabel("Enter the IP-address:");
        JTextField jTextField = new JTextField(15);
        jTextField.addKeyListener(new KeyAdapter() {
            @Override
            public void keyPressed(KeyEvent e) {
                if (e.getKeyCode() == KeyEvent.VK_ENTER) {
                    if (!(jTextField.getText().isEmpty())) {
                        text = jTextField.getText();
                        setVisible(false);
                    }
                }
            }
        });
        jLabel1 = new JLabel("Incorrect address!");
        jLabel1.setForeground(Color.red);
        jLabel1.setVisible(false);
        JPanel jPanel = new JPanel();
        jPanel.add(jLabel, BorderLayout.WEST);
        jPanel.add(jTextField, BorderLayout.EAST);
        jPanel.add(jLabel1, BorderLayout.SOUTH);
        add(jPanel, BorderLayout.CENTER);
        JButton ok = new JButton("OK");
        ok.addActionListener(e -> {
            if (!(jTextField.getText().isEmpty())) {
                text = jTextField.getText();
                setVisible(false);
            }
        });
        JButton cancel = new JButton("Cancel");
        cancel.addActionListener(e -> {
            isBreak = true;
            setVisible(false);
            PlayerModeDialog.getInstance().setVisible(true);
        });
        JPanel panel = new JPanel();
        panel.add(ok);
        panel.add(cancel);
        add(panel, BorderLayout.SOUTH);
        setSize(250, 140);
        setLocationRelativeTo(owner);
    }

    public static ConnectionDialog getInstance() {
        if (connectionDialog == null) connectionDialog = new ConnectionDialog(MainFrame.getInstance());
        return connectionDialog;
    }
    public void setVisibleError(boolean b){
        jLabel1.setVisible(b);
    }
    public String getText() {
        return text;
    }

    public void setBreak(boolean aBreak) {
        isBreak = aBreak;
    }

    public boolean getBreak(){
        return isBreak;
    }
}
