package org.client;

import org.apache.log4j.Logger;

import java.io.IOException;
import java.io.PrintWriter;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.util.Scanner;

/**
 * Created by Alex on 07.12.2017.
 */
public final class ClientSocket extends Socket {
    private static final Logger LOGGER = Logger.getLogger(ClientSocket.class);
    public void setIpAddress(String ipAddress) {
        this.ipAddress = ipAddress;
    }

    private String ipAddress = null;
    private Scanner scanner;
    private PrintWriter printWriter;

    private ClientSocket() {
        super();
    }

    public void setConnection() {
        try {
            this.connect(new InetSocketAddress(ipAddress, 8584), 2000);
            LOGGER.info("Setting connection to Server");
            printWriter = new PrintWriter(this.getOutputStream(), true);
            scanner = new Scanner(this.getInputStream());
        } catch (IOException e) {
            System.err.println("Connection failed");
        }
    }

    public void sendMessage(String s) {
        printWriter.println(s);
        LOGGER.info("Sending message to Server");
    }

    public String acceptMessage() {
        return scanner.nextLine();
    }


    public static synchronized ClientSocket getInstance() {
        return new ClientSocket();
    }

}

