package org.client;

import com.google.gson.Gson;
import org.apache.log4j.Logger;
import org.common.*;
import org.common.buttons.ArrayOfButtons;
import org.common.buttons.ButtonForGame;
import org.common.mainFrame.MainFrame;
import org.common.smallFrames.DisconnectDialog;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class MainClient implements Player {
    private static final Logger LOGGER = Logger.getLogger(MainClient.class);
    private ClientSocket clientSocket = null;
    private Gson gson = new Gson();
    private ArrayOfButtons arrayOfButtons = ArrayOfButtons.getInstance();
    private boolean isCellAvailable = false;
    private Action action = new Action();
    private WinningService winningService;

    @Override
    public void beginGame() {
        LOGGER.info("Starting Client mode");
        MainFrame.getInstance().setTitle("Tic-Tac-Toe (Client)");
        ConnectionDialog connectionDialog = ConnectionDialog.getInstance();

        winningService = new WinningService();
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; ++j) {
                int[] id = {i, j};
                ArrayOfButtons.getInstance().getButtonByCoordinates(i, j).setId(id);
                ArrayOfButtons.getInstance().getButtonByCoordinates(i, j).addActionListener(action);
            }
        }
        clientSocket = ClientSocket.getInstance();
        while (!clientSocket.isConnected()) {
            connectionDialog.setVisible(true);
            if (connectionDialog.getBreak()) {
                ConnectionDialog.getInstance().setBreak(false);
                break;
            }
            String ipAddress = connectionDialog.getText();
            clientSocket.setIpAddress(ipAddress);
            clientSocket.setConnection();
            if (clientSocket.isConnected()) {
                break;
            } else {
                clientSocket = ClientSocket.getInstance();
                ConnectionDialog.getInstance().setVisibleError(true);
            }
        }
        if (clientSocket.isConnected()) {
            new AcceptThread().start();
        }

    }

    @Override
    public void endGame() {
        LOGGER.info("Starting endGame of Client mode");
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                for (ActionListener al : ArrayOfButtons.getInstance().getButtonByCoordinates(i, j).getActionListeners()) {
                    ArrayOfButtons.getInstance().getButtonByCoordinates(i, j).removeActionListener(al);
                }

            }
        }
    }

    private class Action implements ActionListener {
        @Override
        public void actionPerformed(ActionEvent e) {
            ButtonForGame jButton = (ButtonForGame) e.getSource();
            if ((jButton.getText().isEmpty()) && isCellAvailable) {
                LOGGER.info("Adding information to message for Server");
                jButton.setText("O");
                GameMessage gameMessage = new GameMessage(jButton.getId(), jButton.getText(), isCellAvailable);
                clientSocket.sendMessage(gson.toJson(gameMessage));
                isCellAvailable = false;
                winningService.setEndGameText("You win!");
                winningService.resultOfWin(arrayOfButtons);
                new AcceptThread().start();
            }
        }
    }

    private class AcceptThread extends Thread {
        @Override
        public void run() {
            try {
                GameMessage gameMessage = gson.fromJson(clientSocket.acceptMessage(), GameMessage.class);
                LOGGER.info("Accepting message from Server");
                isCellAvailable = gameMessage.processMessage();
                winningService.setEndGameText("You lose!");
                winningService.resultOfWin(arrayOfButtons);
            } catch (Exception e) {
                LOGGER.warn("Disconnecting Server");
                DisconnectDialog.getInstance().setjLabelText("Server disconnected!");
                DisconnectDialog.getInstance().setVisible(true);
            }
        }
    }
}
