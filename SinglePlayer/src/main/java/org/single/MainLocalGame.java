package org.single;

import org.apache.log4j.Logger;
import org.common.WinningService;
import org.common.Player;
import org.common.buttons.ArrayOfButtons;
import org.common.buttons.ButtonForGame;
import org.common.mainFrame.MainFrame;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * Run application start
 */
public class MainLocalGame implements Player{
    private static final Logger LOGGER = Logger.getLogger(MainLocalGame.class);
    private boolean change;
    private WinningService winningService;
    @Override
    public void beginGame() {
        LOGGER.info("Starting beginGame Local mode");
        MainFrame.getInstance().setTitle("Tic-Tac-Toe (Local)");
        winningService = new WinningService();
        winningService.setEndGameText("Victory");
        Action action = new Action();
        change = true;
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                ArrayOfButtons.getInstance().getButtonByCoordinates(i, j).addActionListener(action);
            }
        }
    }

    @Override
    public void endGame() {
        LOGGER.info("Starting endGame Local mode");
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                for (ActionListener al : ArrayOfButtons.getInstance().getButtonByCoordinates(i, j).getActionListeners()){
                    ArrayOfButtons.getInstance().getButtonByCoordinates(i, j).removeActionListener(al);
                }

            }
        }

    }

    // The class is for MainPanel's buttons.
    private class Action implements ActionListener {
        @Override
        public void actionPerformed(ActionEvent e) {
            ButtonForGame jButton = (ButtonForGame) e.getSource();
            if ((jButton.getText().isEmpty())) {
                if (change) {
                    LOGGER.info("Pressed X");
                    jButton.setText("X");
                    winningService.resultOfWin(ArrayOfButtons.getInstance());
                } else {
                    LOGGER.info("Pressed O");
                    jButton.setText("O");
                    winningService.resultOfWin(ArrayOfButtons.getInstance());
                }
                doChange(change);
            }
        }
    }

    private void doChange(boolean change){
        this.change = !change;
    }
}